<?php 
session_start();
error_reporting(0);
$user1=$_SESSION['user'];
$s_id=$_SESSION['s_id'];
?>
<!DOCTYPE html>
<html>
<head>
<style>
.responstable {
  margin: 1em 0;
  width: 100%;
  overflow: hidden;
  background: #FFF;
  color: #024457;
  border-radius: 10px;
  border: 1px solid #167F92;
}
.responstable tr {
  border: 1px solid #D9E4E6;
}
.responstable tr:nth-child(odd) {
  background-color: #EAF3F3;
}
.responstable th {
  display: none;
  border: 1px solid #FFF;
  background-color: #002e6e;
  color: #FFF;
  padding: 0.7em;
}
.responstable th:first-child {
  display: table-cell;
  text-align: center;
}
.responstable th:nth-child(2) {
  display: table-cell;
}
.responstable th:nth-child(2) span {
  display: none;
}
.responstable th:nth-child(2):after {
  content: attr(data-th);
}
@media (min-width: 480px) {
  .responstable th:nth-child(2) span {
    display: block;
  }
  .responstable th:nth-child(2):after {
    display: none;
  }
}
.responstable td {
  display: block;
  word-wrap: break-word;
  max-width: .7em;
}
.responstable td:first-child {
  display: table-cell;
  text-align: center;
  border-right: 1px solid #D9E4E6;
}
@media (min-width: 480px) {
  .responstable td {
    border: 1px solid #D9E4E6;
  }
}
.responstable th, .responstable td {
  text-align: left;
  margin: .5em 1em;
}
@media (min-width: 480px) {
  .responstable th, .responstable td {
    display: table-cell;
    padding: .7em;
  }
}

body {
  
  font-family: Arial, sans-serif;
  color: #024457;
  background: #002e6e;
}

h1 {
  font-family: Verdana;
  font-weight: normal;
  color: #024457;
}
h1 span {
  color: #167F92;
}
.button {
    background-color: #0d635c;
    border: none;
    color: white;
    padding: 4px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
	 border-radius: 4px;
}
</style>
</head>
<body>

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "flavidostore";

// Create connection
     $conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
     if ($conn->connect_error) 
	 {
       die("Connection failed: " . $conn->connect_error);
     } 

    $sql =  "SELECT * FROM file_info where user= '$user1' AND user_id = '$s_id';";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) 
	{
          echo "<table class = responstable><tr><th>ID</th><th>Name</th><th>FILE NAME</th><th>PATH</th><th>PAGES</th><th>PRICE</th><tr>";
         // output data of each row
          $nu = 1;
	      while($row = $result->fetch_assoc()) 
		    {
		      echo "<tr><td>" .$nu. "</td><td>" . $row["user"]. "</td> <td>" . $row["file_name"]. "</td><td>" .$row["path"]."</td><td>" . $row["pages"]."</td><td>" .$row["pages"]."</td></tr>";
		      $nu = $nu +1;
	          $total = $total + $row["pages"];
           }
	     echo "<tr><td colspan=5>TOTAL OF PRICE(INR)</td><td>".$total.".RS</td><tr>";
         echo "</table>";
		
   } 
   else
	{
       echo "0 results";
    }

$conn->close();
?> 
<input type="button" class = "button" style="float: right;" value="PayNow"/>
</body>
</html>