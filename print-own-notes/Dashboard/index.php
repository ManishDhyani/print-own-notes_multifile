<?php
session_start();
$user=$_SESSION['user'];
$s_id=$_SESSION['s_id'];
?>


<!DOCTYPE html>
<!-- saved from url=(0035)https://printster.in/users/register -->
<html><head>
<script src = "angular.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>
<!-- Meta -->


<meta name="author" content="Giga Singh">
<meta name="contact" content="info@printster.in">
<meta name="robots" content="noodp">
<meta http-equiv="copyright" content="Copyright 2017 Printster.in">
<meta property="og:title" content="Printster.in">
<meta property="og:type" content="company">
<meta property="og:description" content="India&#39;s First Online Document Printing Store">
<meta property="og:image" content="images/logo.png">
<meta name="og_url" property="og:url" content="http://www.printster.in/">
<meta name="geo.placename" content="New Delhi, Delhi 110092, India">
<meta name="geo.position" content="28.6300770;77.2740290">
<meta name="geo.region" content="IN-Delhi">
<meta name="ICBM" content="28.6300770, 77.2740290">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="alexaVerifyID" content="F9fTNXtH0eXXSdqj_v1FmQ9XfY">
<meta name="google-site-verification" content="j9AVeFVFj9rko4N6kowYKZukB63geLFroAqRg3Dc7SY">
<meta name="e2ed14da9439d43782e687db25345fbe70649528" content="d4287678a92e8a4b142703d201a0c6079030bedc">
<!--
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    -->
 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>



<!--<base href="/">--><base href=".">
<link rel="canonical" href="http://www.printster.in/">
<link rel="publisher" href="https://plus.google.com/+PrintsterInOfficial">
<meta name="msvalidate.01" content="FC81E3BA39D460942564EBDBF7C039DD">
<link rel="shortcut icon" href="https://cdn.printster.in/favicon/favicon.ico">
<link rel="apple-touch-icon" sizes="152x152" href="https://cdn.printster.in/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="120x120" href="https://cdn.printster.in/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="https://cdn.printster.in/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="60x60" href="https://cdn.printster.in/favicon/apple-icon-60x60.png">

<!-- Stylesheets
	============================================= -->

<link href="../Dashboard/css/css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="../Dashboard/css/bootstrap.min.css" type="text/css">
<!--<link rel="stylesheet/less" href="//cdn.printster.in/css/style.less" type="text/less" />-->
<link rel="stylesheet" href="../Dashboard/css/style.min.css" type="text/css">
<link rel="stylesheet" href="../Dashboard/css/swiper.min.css" type="text/css">
<!--<link rel="stylesheet/less" href="//cdn.printster.in/css/less/dark.less" type="text/less" />-->
<link rel="stylesheet" href="../Dashboard/css/dark.min.css" type="text/css">
<link rel="stylesheet" href="../Dashboard/css/font-icons.min.css" type="text/css">
<link rel="stylesheet" href="../Dashboard/css/animate.min.css" type="text/css">
<link rel="stylesheet" href="../Dashboard/css/magnific-popup.min.css" type="text/css">
<link rel="stylesheet" href="../Dashboard/css/colors.min.css" type="text/css">
<!--<link rel="stylesheet/less" href="//cdn.printster.in/css/less/responsive.less" type="text/less" />-->
<link rel="stylesheet" href="../Dashboard/css/responsive.min.css" type="text/css">
<link rel="stylesheet" href="../Dashboard/css/custom.min.css" type="text/css">
<meta name="viewport" content="width=device-width, initial-scale=1">
<noscript>JavaScript is off. Please enable to view full site.</noscript>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="css/animate.min.css" type="text/css">
<link rel="stylesheet" href="css/animate.min.css" type="text/css">
<link rel="stylesheet" href="css/style.css" type="text/css">






<meta name="twitter:card" content="app">
<meta name="twitter:title" content="Printster.in | Online Printing Service.">
<meta name="twitter:description" content="Print Documents, Posters, Business Card &amp; more using Printster.in | Digital Printing Service.">
<meta name="twitter:app:country" content="IN">

<!-- External JavaScripts
    ============================================= -->

 




<!-- Google Tag Manager -->

<!-- End Google Tag Manager --><!-- Document Title
    ============================================= -->
<title>Signup on Printster.in :: Register yourself an get 10% Discount on Printster.in</title>
<style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style></head><body class="stretched  device-lg">
<!-- Document Wrapper
	============================================= -->
<div id="wrapper" class="clearfix" style="animation-duration: 1.5s; opacity: 1;">
  <!-- Google Tag Manager (noscript) -->
<noscript>&lt;iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5WPX5H"
height="0" width="0" style="display:none;visibility:hidden"&gt;&lt;/iframe&gt;</noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Top Bar
        ============================================= -->

<div class="body-overlay"></div>
<div id="side-panel" class="dark">
  <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="https://printster.in/#"><i class="icon-line-cross"></i></a></div>
  <div class="side-panel-wrap ng-scope">
    <!-- ngIf: User.visitor == 0 -->
    <h2>Notifications</h2>

    <p ng-init="UserBehave()">
    </p><ul>
      <li>Business Query -
        +91-9711133283      </li>
      <li>Technical Query -
        +91-9990903066      </li>
      <li>Delivery Query -
        +91-9899592110      </li>
    </ul>
    <br>
    <p></p>
    <h4>Stay Connected</h4>
    <div class=" clearfix"> <a href="https://www.facebook.com/printster.in" target="_blank" class="social-icon si-small si-borderless nobottommargin si-facebook"> <i class="icon-facebook"></i> <i class="icon-facebook"></i> </a> 
    <a href="https://twitter.com/printsterin" target="_blank" class="social-icon si-small si-borderless nobottommargin si-twitter"> <i class="icon-twitter"></i> <i class="icon-twitter"></i> </a> 
    
    <a href="https://plus.google.com/+PrintsterInOfficial" target="_blank" class="social-icon si-small si-borderless nobottommargin si-gplus"> <i class="icon-gplus"></i> <i class="icon-gplus"></i> </a>
    </div>
  </div>
</div>
<div id="top-bar">
  <div class="col_half nobottommargin hidden-xs clearfix">
    <div class="top-links leftmargin-sm ">
      <ul class="sf-js-enabled clearfix" style="touch-action: pan-y;">
        <li><a href="https://printster.in/information/about-us" target="_blank">About</a></li>
        <li><a href="https://printster.in/blog/" target="_blank"> Blog</a></li>
        <li><a href="https://printster.in/information/frequently-asked-questions-faq" title="FAQ for Printster" target="_blank"> Faqs</a></li>
         <li><a href="https://printster.in/products/printster-price-calculator" title="FAQ for Printster" target="_blank"> Price Calculator</a></li>
        <li><a href="mailto:SUPPORT@FLAVIDO.COM" target="_blank" style="color: #FF0000;" title="Printster.in Email Id for Inquery"><i class="icon-mail"></i> Email : SUPPORT@FLAVIDO.COM</a></li>
      </ul>
    </div>
  </div>
  <div class="col_half  fright  col_last clearfix  nobottommargin">
    <div class="top-links rightmargin-sm">
      <ul class="sf-js-enabled clearfix" style="touch-action: pan-y;">
        <li class="hidden-xs "><a href="https://printster.in/information/career" title="Make your Career with Printster.in" target="_blank"> Careers</a>
        </li><li><a href="https://printster.in/information/printster-offers" title="Get the Printster.in Offers" target="_blank"> Offers</a>
        </li><li><a href="https://printster.in/information/track-order" title="Track Your Order" target="_blank" style="color: #FF0000;"> Track Order</a>
        </li><li ng-hide="User.customerId" class=""><a href="https://printster.in/users/register" ng-click="Signup.Wins = 1"><i class="icon-signin"></i> My Account</a>
          
        </li>
        <li ng-show="User.customerId" class="ng-hide"><a href="https://printster.in/users/informations/" class="ng-binding sf-with-ul"><i class="icon-user2"></i> <span class="capitalize">Hi,</span> <i class="icon-angle-down"></i></a>
          <ul style="display: none;">
            <li><a href="https://printster.in/users/informations/index"><i class="icon-vcard"></i> Account</a></li>
            <li><a href="https://printster.in/users/informations/orders"><i class="icon-like"></i> Orders</a></li>
            <li><a href="https://printster.in/users/informations/documents"><i class="icon-book2"></i> Documents</a></li>
            <li><a href="https://printster.in/users/informations/addresses"><i class="icon-location"></i> Addresses</a></li>
            <li><a href="https://printster.in/" ng-click="Logout();"><i class="icon-signout"></i> Logout</a>
          </li></ul>
      </li></ul>
    </div>
  </div>
</div>
<!-- #top-bar end -->
<header id="header" class="full-header ">
  <div id="header-wrap">
    <div class="container clearfix">
      <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
      <!-- Logo
                    ============================================= -->
      <div id="logo"> <a href="https://printster.in/" class="standard-logo hidden-xs" data-dark-logo="../Dashboard/images/logo-dark.png" data-mobile-logo="//cdn.printster.in/images/po5.png"> <img src="../Dashboard/images/logo.png" alt="Printster.in | Business Cards, Posters, Leaflets | Digital Printing"></a> <a href="https://printster.in/" class="retina-logo " data-dark-logo="//cdn.printster.in/images/logo-dark@2x.png"> <img src="../Dashboard/images/logo@2x.png" alt="Printster.in | Business Cards, Posters, Leaflets | Digital Printing"></a> </div>
      <!-- #logo end --> 
      <!-- Primary Navigation
                    ============================================= -->
      <nav id="primary-menu">
        <ul class="sf-js-enabled" style="touch-action: pan-y;">
          <li class="mega-menu"><a href="https://printster.in/" title="Printster.in Home">Home</a></li>
          <li class="mega-menu sub-menu"><a href="https://printster.in/#" title="Price Calculator for Printing color and Black &amp; White" class="sf-with-ul">
            <div>Products &amp; Pricing</div>
            </a>
            <div class="mega-menu-content style-2 clearfix" style="display: none; width: 1289px;">
              <ul class="mega-menu-column col-md-3" style="">
                <li class="mega-menu-title sub-menu"><a href="https://printster.in/order/select-product" title="Printing Featured Product" class="sf-with-ul">
                  <div>Printing Products</div>
                  </a>
                  <ul style="display: none;">
                    <li><a href="https://printster.in/products/information/document-printing" title="Online Document Printing, Color Printing or Black and White Printing">
                      <div>Document Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/letterhead-printing" title="Letterhead Printing Online">
                      <div>Letterhead Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/leaflets-flyer-printing" title="Print Flyer, Leaflets">
                      <div>Leaflets &amp; Flyers Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/poster-printing" title="Cheap Poster Printing Online">
                      <div>Poster Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/certificates-printing" title="Certificate Printing Online">
                      <div>Certificates printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/dissertation-and-thesis-printing" title="Thesis/dissertation printing and binding ">
                      <div>Dissertation or Thesis Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/catalogue-printing" title="custom catalog printing, catalog printing service">
                      <div>Catalogue Printing</div>
                      </a></li>
                  </ul>
                </li>
              </ul>
              <ul class="mega-menu-column col-md-3" style="">
                <li class="mega-menu-title sub-menu"><a href="https://printster.in/order/select-product" title="Printed Products, Digital Printing Services" class="sf-with-ul">
                  <div>Printing Products</div>
                  </a>
                  <ul style="display: none;">
                    <li><a href="https://printster.in/products/information/presentation-printing" title="Presentations | Presentation Printing | Printster.in">
                      <div>Presentation Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/report-printing" title="Report Printing &amp; Binding Services - Print Your Reports - Printster.in">
                      <div>Report Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/a3-printing" title="A3 Printing Services - Print A3 Posters &amp; Documents Online - Printster.in">
                      <div>A3 Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/newsletter-printing" title="High-Quality Newsletter Printing Services">
                      <div>Newsletter Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/proposal-printing" title="Proposal Printing Solutions - Printster.in">
                      <div>Proposal Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/manual-printing" title="Manual Printing | Custom Manuals | Printster.in">
                      <div>Manual Printing</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/booklet-printing" title="Online Booklet Printing, Full-Color Custom Booklets - Printster">
                      <div>Booklet Printing</div>
                      </a></li>
                  </ul>
                </li>
              </ul>
              <ul class="mega-menu-column col-md-3" style="">
                <li class="mega-menu-title sub-menu"><a href="https://printster.in/order/select-product" title="Online Printing and Binding" class="sf-with-ul">                  <div>Binding Options</div>
                  </a>
                  <ul style="display: none;">
                    <li><a href="https://printster.in/products/information/corner-stapled-binding" title="Print and Staple at Corner">
                      <div>Corner Stapled Binding</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/stapled-binding" title="Print and 3 Stapled Pin on left side">
                      <div>Stapled Binding</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/saddle-stitch-binding" title=" Two or more staples placed along a folded edge">
                      <div>Saddle Stitch Binding</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/spiral-binding" title="spiral binding printster">
                      <div>Spiral Binding</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/wiro-binding" title="Wiro Binding and Twin Loop Binding">
                      <div>Wiro Binding</div>
                      </a></li>
                   <li><a href="https://printster.in/products/information/perfect-binding-or-soft-cover-binding" title="Perfect Books Binding or Soft Cover Binding">
                      <div>Soft Cover Binding </div>
                      </a></li>
                       <li><a href="https://printster.in/products/information/glue-binding" title="Cheap Binding for daily Use">
                      <div>Glue Binding </div>
                      </a></li>
                  </ul>
                </li>
              </ul>
              <ul class="mega-menu-column col-md-3" style="">
                <li class="mega-menu-title sub-menu"><a href="https://printster.in/order/select-product" title="Online Printing and Binding" class="sf-with-ul">                  <div>Binding Options</div>
                  </a>
                  <ul style="display: none;">
                    <li><a href="https://printster.in/products/information/hard-binding" title="Hard Binding or Har Bound or Thesis Binding @ Printster.in">
                      <div>Hard Binding</div>
                      </a></li>
                    <li><a href="https://printster.in/products/information/hard-binding-with-golden-print" title="Thesis Book Binding Golden Embossing ">
                      <div>Hard Binding with Golden Print</div>
                      </a></li>
                     
                  </ul>
                </li>
              </ul>
            </div>
          </li>
          <li><a href="https://printster.in/information/hows-it-works">
            <div>How it Works?</div>
            </a> </li>
          <li> <a ng-show="User.customerId" href="https://printster.in/order/select-product?product=0&amp;pid=0" style="background:#009933; color:#FFFFFF" class="ng-hide">
            <div>Start Printing</div>
            </a> <a ng-hide="User.customerId" href="https://printster.in/users/register" style="background:#009933; color:#FFFFFF" class="">
            <div>Start Printing</div>
            </a> </li>
        </ul>
        
        <!-- Top Cart
                        ============================================= -->
        
        <div id="top-cart" class="hidden-xs  ng-scope"> <a href="https://printster.in/#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><!-- ngIf: totalproducts != '0' --></a>
          <!-- ngIf: totalproducts != '0' -->
        </div>
        
        <!-- #top-cart end -->
     <!--   <div id="top-cart" class="side-panel-trigger"><a href="#"><i class="icon-bell"></i><span >1</span>&nbsp;</a></div>-->
      </nav>
      <!-- #primary-menu end --> 
    </div>
  </div>
</header>
  <!-- Slider
		============================================= -->
 <section id="slider" class="slider-parallax full-screen slider-parallax-visible" style="background: url(&quot;//cdn.printster.in/images/slider/rev/ken-2.jpg&quot;) center center; overflow: visible; height: 527px; transform: translateY(0px);">
		
	<div class="row">
  
  
  <div class="col-md-4" style="background-color:white;">
  
  	<ul class="nav nav-pills nav-stacked">
  <li class="active" id="Home"><a href="#"><span class="glyphicon glyphicon-chevron-right"></span> Home</a></li>
  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span> Upload File</a></li>
  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span> File specification</a></li>
  <li id="pay"><a href="#"><span class="glyphicon glyphicon-chevron-right" ></span> Payment conformation</a></li>
  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span> Procede to pay</a></li>
  <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span> Details</a></li>
</ul>
  
  
  </div>
  

  <div class="col-md-8">

  <span>Welcome: </span><?php echo $user; ?><a href="../Dashboard/logout.php" style= "color  : red; float: right;">(Log Out)</a>
  <div id="upload">
  <body ng-app="myapp">

 <div ng-controller="userCtrl">
      
    <center>  <input type='file' class="button" id='file' name='file' ng-file="uploadfiles">
      <button class="button" id='upload_button' ng-click="upload()">Upload</button><br>
     <!-- <div id= "spinner" style = "display:none"> <i class="fa fa-spinner fa-spin" style="font-size:24px"></i> </div> -->
      
     <p><b>{{response.ext}}<b></p>
	 <!-- <p>{{response.pageCount}}<p>
	  <p>{{response.location}}<p>
	  <p>{{response.user}}<p> -->
	  
	  <!-- Table includes here-->
	  <?php
	   $allowed =  array('gif','png' ,'jpg');
    $file = $_FILES['file']['name'];
    $ext = pathinfo($file, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) 
            { 
//?>
    <script>
	$(document).ready(function(){
       alert('file extension not allowed');
       
	});
    </script>

//<?php
exit(0);
    }
	  ?>
	 
	  <center>
	    <div id="table1" style= "display:none;">
        </div>
	 </center> 

  <!-- Script -->
  <script>
    var upload = angular.module('myapp', []);
    upload.directive('ngFile', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              element.bind('change', function(){

              $parse(attrs.ngFile).assign(scope,element[0].files)
                 scope.$apply();
              });
           }
        };
    }]);

    upload.controller('userCtrl', ['$scope', '$http', function ($scope, $http) {
        
            $scope.upload = function(value){
                var fd=new FormData();
                angular.forEach($scope.uploadfiles,function(file){
                    fd.append('file',file);
                });
       
                $http({
                    method: 'post',
                    url: 'upload.php',
                    data: fd,
                    headers: {'Content-Type': undefined},
                }).then(function successCallback(response) { 
                    // Store response data
                    $scope.response = response.data;
					//table store
					 $("#table1").show();
                     $('#table1').load('table2.php');
					
                });
        }
  
    }
]);

      </script>



       </div>
     </body>  
   </div>
<!-- File upload section is finished here -->

<!-- Payment table is including here -->
<div id="table" style= "display:none;">

</div>

</div>
<!-- Payment table is ending here -->

  
</div>
 
  </div>
 
  </div>	
				
</section>
		
		
  <!-- #slider end -->
</div>
<!-- #wrapper end -->
<!-- #content end -->
<!-- Footer Scripts
    ============================================= -->
<!-- JavaScript Data --> 
<script defer="defer" type="text/javascript" src="../Dashboard/js/jquery.js"></script>
<script defer="defer" type="text/javascript" src="../Dashboard/js/plugins.js"></script>
<script defer="defer" data-require="ui-bootstrap@*" data-semver="0.12.1" src="../Dashboard/js/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script defer="defer" src="../Dashboard/js/angular-resource.min.js"></script>
<script defer="defer" src="../Dashboard/js/angular-cookies.js"></script>
<script defer="defer" type="../Dashboard/js/jquery.dmap.js"></script>
<script>window.data = { refrer: document.referrer,  currenturl: document.URL, pid: '1' }; </script>



<!-- Database entery code script -->




<script>
$(document).ready(function(){
    $("#pay").click(function(){
        $("#upload").hide();
    });
    $("#pay").click(function(){
        $("#table").show();
        $('#table').load('table1.php');
    });
	  
	 $("#Home").click(function(){
        $("#upload").show();
		$("#table").hide();
        
    });
       
    });

</script>



</body></html>